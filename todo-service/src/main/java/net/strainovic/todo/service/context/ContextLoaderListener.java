package net.strainovic.todo.service.context;

import com.mchange.v2.c3p0.PooledDataSource;
import net.strainovic.todo.model.util.LogHelper;
import net.strainovic.todo.solr.RebuildSolrIndexService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.sql.DataSource;

public class ContextLoaderListener extends
        org.springframework.web.context.ContextLoaderListener {

    private static final Log LOG = LogFactory
            .getLog(ContextLoaderListener.class);

    private static final String TODO_JNDI = "java:comp/env/jdbc/todo"; //$NON-NLS-1$

    @Override
    public void contextDestroyed(ServletContextEvent event) {

        super.contextDestroyed(event);

        try {
            InitialContext initialContext = new InitialContext();
            DataSource pdDataSource = (DataSource) initialContext
                    .lookup(TODO_JNDI);

            if (pdDataSource != null
                    && pdDataSource instanceof PooledDataSource) {
                ((PooledDataSource) pdDataSource).close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            super.contextInitialized(event);
            WebApplicationContext context = ContextLoader
                    .getCurrentWebApplicationContext();

            RebuildSolrIndexService rebuildSolrIndexService = getRebuildSolrIndexService(context);

            if (rebuildSolrIndexService.rebuildNeeded()) {
                rebuildSolrIndexService.rebuildIndex();
            }
        } catch (Exception e) {
            LogHelper.error(LOG, "Error occurred when trying to start application!", e);

            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    protected WebApplicationContext createWebApplicationContext(ServletContext sc) {
        return super.createWebApplicationContext(sc);
    }

    private RebuildSolrIndexService getRebuildSolrIndexService(
            WebApplicationContext context) {
        return context.getBean(RebuildSolrIndexService.class);
    }
}
