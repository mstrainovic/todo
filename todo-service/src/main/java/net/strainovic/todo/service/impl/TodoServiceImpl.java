package net.strainovic.todo.service.impl;

import net.strainovic.todo.dao.GenericDAO;
import net.strainovic.todo.dao.TodoDAO;
import net.strainovic.todo.dao.provider.TodoLoadProvider;
import net.strainovic.todo.model.Todo;
import net.strainovic.todo.model.search.SearchRequest;
import net.strainovic.todo.service.TodoService;
import net.strainovic.todo.solr.SearchSolrIndexService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("todoService")
public class TodoServiceImpl extends BaseGenericService<Todo, Long> implements
        TodoService {

    @Autowired
    private SearchSolrIndexService searchSolrIndexService;

    @Autowired
    private TodoDAO todoDAO;

    private SearchSolrIndexService getSearchSolrIndexService() {
        return searchSolrIndexService;
    }

    private TodoDAO getTodoDAO() {
        return todoDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Todo> search(SearchRequest searchRequest) {
        List<Todo> result = new ArrayList<>();

        List<Long> todoIds = getSearchSolrIndexService().search(searchRequest);

        if (CollectionUtils.isNotEmpty(todoIds)) {
            result.addAll(getTodoDAO().loadAll(TodoLoadProvider.create().todoId(todoIds)));
        }
        return result;
    }

    @Override
    @Value("#{todoDAO}")
    public void setEntityDao(GenericDAO<Todo, Long> entityDao) {
        super.setEntityDao(entityDao);
    }
}
