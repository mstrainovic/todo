package net.strainovic.todo.service.impl;

import net.strainovic.todo.dao.GenericDAO;
import net.strainovic.todo.dao.LoadProvider;
import net.strainovic.todo.model.Identifiable;
import net.strainovic.todo.service.GenericService;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

public class BaseGenericService<T extends Identifiable<G>, G extends Serializable>
        implements GenericService<T, G> {

    private GenericDAO<T, G> _entityDao;

    @Override
    @Transactional
    public void delete(T entity) {
        getEntityDao().delete(entity);
    }

    protected final void flush() {
        getEntityDao().flush();
    }

    private GenericDAO<T, G> getEntityDao() {
        return _entityDao;
    }

    @Override
    @Transactional(readOnly = true)
    public T load(LoadProvider<T> loadProvider) {
        return getEntityDao().load(loadProvider);
    }

    @Override
    @Transactional(readOnly = true)
    public List<T> loadAll(LoadProvider<T> loadProvider) {
        return getEntityDao().loadAll(loadProvider);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> loadIds(LoadProvider<T> loadProvider) {
        return getEntityDao().loadIds(loadProvider);
    }

    @Transactional
    public final T save(T entity) {

        T dtoEntity = getEntityDao().save(entity);

        return dtoEntity;
    }

    public void setEntityDao(GenericDAO<T, G> entityDao) {
        _entityDao = entityDao;
    }

    @Override
    @Transactional
    public final void update(T entity) {
        getEntityDao().update(entity);
    }
}
