package net.strainovic.todo.service.impl;

import net.strainovic.todo.dao.GenericDAO;
import net.strainovic.todo.model.User;
import net.strainovic.todo.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl extends BaseGenericService<User, Long> implements
        UserService {

    @Override
    @Value("#{userDAO}")
    public void setEntityDao(GenericDAO<User, Long> entityDao) {
        super.setEntityDao(entityDao);
    }
}
