package net.strainovic.todo.service;

import net.strainovic.todo.model.Todo;
import net.strainovic.todo.model.search.SearchRequest;

import java.util.List;

public interface TodoService extends GenericService<Todo, Long> {

    List<Todo> search(SearchRequest searchRequest);

}
