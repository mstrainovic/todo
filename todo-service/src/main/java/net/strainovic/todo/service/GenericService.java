package net.strainovic.todo.service;

import net.strainovic.todo.dao.LoadProvider;
import net.strainovic.todo.model.Identifiable;

import java.io.Serializable;
import java.util.List;

public interface GenericService<T extends Identifiable<ID>, ID extends Serializable> {

    void delete(T entity);

    T load(LoadProvider<T> loadProvider);

    List<T> loadAll(LoadProvider<T> loadProvider);

    List<Long> loadIds(LoadProvider<T> loadProvider);

    T save(T entity);

    void update(T entity);

}
