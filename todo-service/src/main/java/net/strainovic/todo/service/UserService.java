package net.strainovic.todo.service;

import net.strainovic.todo.model.User;

public interface UserService extends GenericService<User, Long> {
}
