package net.strainovic.todo.solr.impl;

import net.strainovic.todo.solr.adapters.CurrentContextAdapter;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class SolrClientInitializingBean implements DisposableBean,
        InitializingBean {

    static final String ALL_QUERY = "*:*"; //$NON-NLS-1$
    static final METHOD DEFAULT_METHOD = METHOD.POST;
    static final String ID_QUERY_FORMAT = "_id_:%s"; //$NON-NLS-1$
    static final String SEARCH_PATH = "/search"; //$NON-NLS-1$
    static final String UPDATE_PATH = "/update"; //$NON-NLS-1$

    @Autowired
    public CurrentContextAdapter _currentContext;

    private SolrClient _client;

    @Override
    public void afterPropertiesSet() throws Exception {
        _client = getCurrentContext().initSolrClient();
        ping(_client);
    }

    @Override
    public void destroy() throws Exception {
        getCurrentContext().shutdownClient(_client);
    }

    protected SolrClient getClient() {
        return _client;
    }

    protected CurrentContextAdapter getCurrentContext() {
        return _currentContext;
    }

    private void ping(SolrClient client) {
        try {
            SolrQuery query = new SolrQuery(ALL_QUERY);
            query.setRows(0);
            QueryRequest req = new QueryRequest(query, DEFAULT_METHOD);
            req.setPath(SEARCH_PATH);

            req.process(client);
        } catch (Exception e) {
            throw new RuntimeException("Cannot ping solr server");
        }
    }

}
