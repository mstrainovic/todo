package net.strainovic.todo.solr;

import net.strainovic.todo.solr.model.HttpClientConfiguration;
import org.apache.http.impl.client.CloseableHttpClient;

public interface HttpClientFactory {

    CloseableHttpClient createHttpClient(HttpClientConfiguration configuration);

}
