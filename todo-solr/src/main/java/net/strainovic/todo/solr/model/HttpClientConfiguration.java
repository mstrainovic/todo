package net.strainovic.todo.solr.model;

public class HttpClientConfiguration {
    private int _connectionTimeout = 10000;
    private boolean _handleSessionId = false;
    private int _maxRetry = 3;
    private int _requestTimeout = 10000;
    private int _retryDelay = 100;
    private int _soTimeout = 180000;
    private int _totalConnections = 1000;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HttpClientConfiguration other = (HttpClientConfiguration) obj;
        if (_connectionTimeout != other._connectionTimeout)
            return false;
        if (_handleSessionId != other._handleSessionId)
            return false;
        if (_maxRetry != other._maxRetry)
            return false;
        if (_requestTimeout != other._requestTimeout)
            return false;
        if (_retryDelay != other._retryDelay)
            return false;
        if (_soTimeout != other._soTimeout)
            return false;
        if (_totalConnections != other._totalConnections)
            return false;
        return true;
    }

    public int getConnectionTimeout() {
        return _connectionTimeout;
    }

    public int getMaxRetry() {
        return _maxRetry;
    }

    public int getRequestTimeout() {
        return _requestTimeout;
    }

    public int getRetryDelay() {
        return _retryDelay;
    }

    public int getSoTimeout() {
        return _soTimeout;
    }

    public int getTotalConnections() {
        return _totalConnections;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + _connectionTimeout;
        result = prime * result + (_handleSessionId ? 1231 : 1237);
        result = prime * result + _maxRetry;
        result = prime * result + _requestTimeout;
        result = prime * result + _retryDelay;
        result = prime * result + _soTimeout;
        result = prime * result + _totalConnections;
        return result;
    }

    public boolean isHandleSessionId() {
        return _handleSessionId;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        _connectionTimeout = connectionTimeout;
    }

    public void setHandleSessionId(boolean handleSessionId) {
        _handleSessionId = handleSessionId;
    }

    public void setMaxRetry(int maxRetry) {
        _maxRetry = maxRetry;
    }

    public void setRequestTimeout(int requestTimeout) {
        _requestTimeout = requestTimeout;
    }

    public void setRetryDelay(int retryDelay) {
        _retryDelay = retryDelay;
    }

    public void setSoTimeout(int soTimeout) {
        _soTimeout = soTimeout;
    }

    public void setTotalConnections(int totalConnections) {
        _totalConnections = totalConnections;
    }

    @Override
    public String toString() {
        return "HttpClientConfiguration [_maxRetry=" + _maxRetry
                + ", _totalConnections=" + _totalConnections + ", _retryDelay="
                + _retryDelay + ", _connectionTimeout=" + _connectionTimeout
                + ", _requestTimeout=" + _requestTimeout + ", _soTimeout="
                + _soTimeout + ", _hanldeSessionId=" + _handleSessionId + "]";
    }
}