package net.strainovic.todo.solr.impl;

import net.strainovic.todo.model.util.LogHelper;
import net.strainovic.todo.solr.SolrOperations;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("solrOperations")
public class SolrOperationsImpl extends SolrClientInitializingBean implements
        SolrOperations {

    private static final Log LOG = LogFactory.getLog(SolrOperationsImpl.class);

    @Override
    public void add(List<SolrInputDocument> docs, boolean makeVisible) {
        if (CollectionUtils.isNotEmpty(docs)) {
            UpdateRequest req = new UpdateRequest(UPDATE_PATH);
            if (makeVisible) {
                req.setAction(UpdateRequest.ACTION.COMMIT, false, true, true);
            }
            if (CollectionUtils.isNotEmpty(docs)) {
                req.add(docs);
            }
            try {
                req.process(getClient());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void commit() {
        UpdateRequest req = new UpdateRequest(UPDATE_PATH);
        req.setAction(UpdateRequest.ACTION.COMMIT, false, true, true);

        try {
            req.process(getClient());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteAll(boolean makeVisible) {
        try {
            UpdateRequest req = new UpdateRequest(UPDATE_PATH);
            req.deleteByQuery(ALL_QUERY);
            if (makeVisible) {
                req.setAction(UpdateRequest.ACTION.COMMIT, false, true, true);
            }
            req.process(getClient());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void deleteById(Long id, boolean makeVisible) {
        try {
            if (id == null) {
                return;
            }
            LogHelper.debug(LOG, String.format("Deleting solr entry ID: %s", id.toString()));

            UpdateRequest req = new UpdateRequest(UPDATE_PATH);
            req.deleteByQuery(String.format(ID_QUERY_FORMAT, id.toString()));
            if (makeVisible) {
                req.setAction(UpdateRequest.ACTION.COMMIT, false, true, true);
            }
            req.process(getClient());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public DocumentObjectBinder getBinder() {
        return getClient().getBinder();
    }

    @Override
    public QueryResponse query(SolrQuery query) {
        try {
            QueryRequest req = new QueryRequest(query, DEFAULT_METHOD);
            req.setPath(SEARCH_PATH);
            return req.process(getClient());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
