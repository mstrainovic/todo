package net.strainovic.todo.solr.impl;

import net.strainovic.todo.dao.TodoDAO;
import net.strainovic.todo.dao.provider.TodoLoadProvider;
import net.strainovic.todo.dao.utils.ChunkedExecutionHelper;
import net.strainovic.todo.model.Todo;
import net.strainovic.todo.model.util.LogHelper;
import net.strainovic.todo.solr.RebuildSolrIndexService;
import net.strainovic.todo.solr.SolrOperations;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static net.strainovic.todo.solr.fields.SolrFieldConstants.TODO_NOTE;
import static net.strainovic.todo.solr.fields.SolrFieldConstants.UNIQUE_ID;
import static net.strainovic.todo.solr.fields.SolrQueryConstants.ANY;
import static net.strainovic.todo.solr.fields.SolrQueryConstants.FIELD_NAME_SEPARATOR;

@Service("rebuildSolrIndexService")
public class RebuildSolrIndexServiceImpl implements RebuildSolrIndexService {

    private static final Log LOG = LogFactory
            .getLog(RebuildSolrIndexServiceImpl.class);
    @Autowired
    private SolrOperations _solrOperations;
    @Autowired
    private TodoDAO _todoDAO;

    @Override
    public void deleteAll() {
        getSolrOperations().deleteAll(Boolean.TRUE);
    }

    @Override
    public void deleteById(Long id) {
        getSolrOperations().deleteById(id, true);
    }

    private SolrOperations getSolrOperations() {
        return _solrOperations;
    }

    public TodoDAO getTodoDAO() {
        return _todoDAO;
    }

    private void indexTodos(final Collection<Long> todoIds) {
        LogHelper.debug(LOG, "SOLR rebuild start.");

        final MutableInt batchCounter = new MutableInt(0), progress = new MutableInt(
                0);
        final int updateBatchSize = ChunkedExecutionHelper.DEFAULT_CHUNK_SIZE;

        final List<SolrInputDocument> todoDocs = new ArrayList<>();

        ChunkedExecutionHelper
                .executeChuncked(
                        todoIds,
                        chunk -> {
                            List<Todo> todos = getTodoDAO().loadAll(TodoLoadProvider.create().todoId(chunk));

                            for (Todo todo : todos) {
                                batchCounter.increment();

                                SolrInputDocument doc = new SolrInputDocument();

                                doc.addField(UNIQUE_ID, todo.getTodoId());
                                doc.addField(TODO_NOTE, todo.getNote());

                                todoDocs.add(doc);

                                if (batchCounter.intValue() >= updateBatchSize) {
                                    getSolrOperations().add(todoDocs, false);
                                    todoDocs.clear();
                                    batchCounter.setValue(0);
                                }
                            }

                            progress.setValue(progress.longValue()
                                    + chunk.size());
                            LogHelper.debug(
                                    LOG,
                                    "Rebuild progress: %s%%",
                                    progress.longValue() * 100
                                            / todoIds.size());
                        }
                );

        if (CollectionUtils.isNotEmpty(todoDocs)) {
            getSolrOperations().add(todoDocs, true);
        } else {
            LogHelper.trace(LOG, "Todo Documents is empty do commit.");
            getSolrOperations().commit();
        }
        LogHelper.debug(LOG, "SOLR rebuild finish.");
    }

    @Override
    @Transactional(readOnly = true)
    public void rebuildIndex() {
        List<Long> todoIds = getTodoDAO().loadIds(TodoLoadProvider.create());

        indexTodos(todoIds);
    }

    @Override
    @Transactional(readOnly = true)
    public void rebuildIndex(Collection<Long> ids) {
        List<Long> todoIds = getTodoDAO().loadIds(TodoLoadProvider.create().todoId(ids));

        indexTodos(todoIds);
    }

    @Override
    @Transactional(readOnly = true)
    public void rebuildIndex(Long id) {
        List<Long> todoIds = getTodoDAO().loadIds(TodoLoadProvider.create().todoId(id));

        indexTodos(todoIds);
    }

    @Override
    public boolean rebuildNeeded() {
        SolrOperations solrOperations = getSolrOperations();

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(ANY);
        queryBuilder.append(FIELD_NAME_SEPARATOR);
        queryBuilder.append(ANY);

        SolrQuery query = new SolrQuery(queryBuilder.toString());
        query.setRows(0);

        QueryResponse response = solrOperations.query(query);

        return response.getResults().getNumFound() == 0;
    }
}
