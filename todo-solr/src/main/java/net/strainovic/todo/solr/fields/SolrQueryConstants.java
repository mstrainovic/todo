package net.strainovic.todo.solr.fields;

public class SolrQueryConstants {

    public static final String ANY = "*";
    public static final String FIELD_NAME_SEPARATOR = ":";
}
