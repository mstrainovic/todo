package net.strainovic.todo.solr;


import java.util.Collection;

public interface RebuildSolrIndexService {

    void deleteAll();

    void deleteById(Long id);

    void rebuildIndex();

    void rebuildIndex(Long id);

    void rebuildIndex(Collection<Long> ids);

    boolean rebuildNeeded();
}
