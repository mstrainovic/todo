package net.strainovic.todo.solr;


import net.strainovic.todo.model.search.SearchRequest;

import java.util.List;

public interface SearchSolrIndexService {

    List<Long> search(SearchRequest searchRequest);

}
