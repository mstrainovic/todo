package net.strainovic.todo.solr.fields;

public class SolrFieldConstants {

    public static final String TODO_NOTE = "_todoNote_t_is";

    public static final String UNIQUE_ID = "_id_";

}
