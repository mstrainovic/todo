package net.strainovic.todo.solr;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;

import java.util.List;

public interface SolrOperations {

    void add(List<SolrInputDocument> docs, boolean makeVisable);

    void commit();

    void deleteAll(boolean makeVisable);

    void deleteById(Long id, boolean makeVisable);

    DocumentObjectBinder getBinder();

    QueryResponse query(SolrQuery query);

}
