package net.strainovic.todo.solr.model;


public abstract class SolrConnectionConfigurer {

    private String _core;

    private String _url;

    public String getCore() {
        return _core;
    }

    public String getUrl() {
        return _url;
    }

    protected <T> T nullSafePropertyValue(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    public void setCore(String core) {
        _core = core;
    }

    public void setUrl(String url) {
        _url = url;
    }

}
