package net.strainovic.todo.solr.impl;

import net.strainovic.todo.model.search.SearchRequest;
import net.strainovic.todo.solr.SearchSolrIndexService;
import net.strainovic.todo.solr.SolrOperations;
import net.strainovic.todo.solr.fields.SolrFieldConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static net.strainovic.todo.solr.fields.SolrQueryConstants.ANY;
import static net.strainovic.todo.solr.fields.SolrQueryConstants.FIELD_NAME_SEPARATOR;

@Service("searchSolrIndexServiceImpl")
public class SearchSolrIndexServiceImpl implements SearchSolrIndexService {

    @Autowired
    private SolrOperations solrOperations;

    private SolrOperations getSolrOperations() {
        return solrOperations;
    }

    @Override
    public List<Long> search(SearchRequest searchRequest) {
        List<Long> result = new ArrayList<>();

        SolrQuery query = new SolrQuery();

        String searchText = searchRequest.getTodoText();

        StringBuilder queryBuilder = new StringBuilder();
        if (StringUtils.isNotBlank(searchText)) {
            queryBuilder.append(SolrFieldConstants.TODO_NOTE);
            queryBuilder.append(FIELD_NAME_SEPARATOR);
            queryBuilder.append(searchText);
        } else {
            queryBuilder.append(ANY);
            queryBuilder.append(FIELD_NAME_SEPARATOR);
            queryBuilder.append(ANY);
        }

        query.setQuery(queryBuilder.toString());

        QueryResponse response = getSolrOperations().query(query);

        SolrDocumentList solrDocuments = response.getResults();
        if (CollectionUtils.isNotEmpty(solrDocuments)) {
            result.addAll(solrDocuments.stream().map(document -> (Long) document.get(SolrFieldConstants.UNIQUE_ID)).collect(Collectors.toList()));
        }

        return result;
    }
}
