package net.strainovic.todo.solr.adapters;

import com.mysql.jdbc.Messages;
import net.strainovic.todo.solr.impl.HttpClientFactory;
import net.strainovic.todo.solr.model.HttpClientConfiguration;
import net.strainovic.todo.solr.model.SingleNodeSolrConnectionConfigurer;
import net.strainovic.todo.solr.model.SolrConnectionConfigurer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.BinaryRequestWriter;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;

public class CurrentContextAdapter {

    private SolrConnectionConfigurer _connectionConfigurer;

    private HttpClientConfiguration getHttpClientConfiguration(
            SingleNodeSolrConnectionConfigurer connectionConfigurer) {
        HttpClientConfiguration result = new HttpClientConfiguration();
        result.setMaxRetry(connectionConfigurer.getHttpMaxRetry());
        result.setRetryDelay(connectionConfigurer.getHttpRetryDelay());
        result.setTotalConnections(connectionConfigurer.getHttpMaxConnections());
        result.setConnectionTimeout(connectionConfigurer
                .getHttpConnectionTimeout());
        result.setRequestTimeout(connectionConfigurer.getHttpRequestTimeout());
        result.setSoTimeout(connectionConfigurer.getHttpSoTimeout());
        return result;
    }

    public SolrClient initSolrClient() {

        SolrConnectionConfigurer connectionConfigurer;
        try {
            connectionConfigurer = (SingleNodeSolrConnectionConfigurer) InitialContext
                    .doLookup(SingleNodeSolrConnectionConfigurer.SOLR_CONNECTION_JNDI_RESOURCE_NAME);
        } catch (NamingException e) {
            throw new RuntimeException("Cannot find single node SOLR context connection configuration");
        }
        return initializeSolrClient((SingleNodeSolrConnectionConfigurer) connectionConfigurer);
    }

    private SolrClient initializeSolrClient(
            SingleNodeSolrConnectionConfigurer connectionConfigurer) {
        setConnectionConfigurer(connectionConfigurer);

        String externalUrl = connectionConfigurer.getUrl();
        Validate.notNull(externalUrl);

        String core = connectionConfigurer.getCore();
        Validate.notNull(core);

        String coreURL = externalUrl.concat(
                externalUrl.endsWith("/") ? StringUtils.EMPTY
                        : "/").concat(core);

        CloseableHttpClient httpClient = HttpClientFactory.getInstance()
                .createHttpClient(
                        getHttpClientConfiguration(connectionConfigurer));

        HttpSolrClient solrClient = new HttpSolrClient(coreURL, httpClient);
        solrClient.setRequestWriter(new BinaryRequestWriter());
        solrClient.setUseMultiPartPost(connectionConfigurer
                .getHttpUseMultipart());

        return solrClient;
    }

    private void setConnectionConfigurer(
            SolrConnectionConfigurer connectionConfigurer) {
        _connectionConfigurer = connectionConfigurer;
    }

    public void shutdownClient(SolrClient solrClient) {
        try {
            solrClient.close();
        } catch (IOException e) {
            throw new RuntimeException(
                    Messages.getString("CurrentContextAdapter.0"), e); //$NON-NLS-1$
        }
    }

}
