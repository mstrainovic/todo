package net.strainovic.todo.solr.model;


public class SingleNodeSolrConnectionConfigurer extends
        SolrConnectionConfigurer {

    public static final String SOLR_CONNECTION_JNDI_RESOURCE_NAME = "java:comp/env/solr/todo";

    private static final int DEFAULT_HTTP_CONNECTION_TIMEOUT = 10000;

    private static final int DEFAULT_HTTP_MAX_CONNECTIONS = 100;

    private static final int DEFAULT_HTTP_MAX_RETRY = 5;

    private static final int DEFAULT_HTTP_REQUEST_TIMEOUT = 10000;

    private static final int DEFAULT_HTTP_RETRY_DELAY = 1000;

    private static final int DEFAULT_HTTP_SO_TIMEOUT = 10000;

    private static final boolean DEFAULT_HTTP_USE_MULTIPART = Boolean.TRUE;

    private Integer _httpConnectionTimeout;

    private Integer _httpMaxConnections;

    private Integer _httpMaxRetry;

    private Integer _httpRequestTimeout;

    private Integer _httpRetryDelay;

    private Integer _httpSoTimeout;

    private Boolean _httpUseMultipart;

    public Integer getHttpConnectionTimeout() {
        return nullSafePropertyValue(_httpConnectionTimeout,
                DEFAULT_HTTP_CONNECTION_TIMEOUT);
    }

    public Integer getHttpMaxConnections() {
        return nullSafePropertyValue(_httpMaxConnections,
                DEFAULT_HTTP_MAX_CONNECTIONS);
    }

    public Integer getHttpMaxRetry() {
        return nullSafePropertyValue(_httpMaxRetry, DEFAULT_HTTP_MAX_RETRY);
    }

    public Integer getHttpRequestTimeout() {
        return nullSafePropertyValue(_httpRequestTimeout,
                DEFAULT_HTTP_REQUEST_TIMEOUT);
    }

    public Integer getHttpRetryDelay() {
        return nullSafePropertyValue(_httpRetryDelay, DEFAULT_HTTP_RETRY_DELAY);
    }

    public Integer getHttpSoTimeout() {
        return nullSafePropertyValue(_httpSoTimeout, DEFAULT_HTTP_SO_TIMEOUT);
    }

    public Boolean getHttpUseMultipart() {
        return nullSafePropertyValue(_httpUseMultipart,
                DEFAULT_HTTP_USE_MULTIPART);
    }

    public void setHttpConnectionTimeout(Integer httpConnectionTimeout) {
        _httpConnectionTimeout = httpConnectionTimeout;
    }

    public void setHttpMaxConnections(Integer httpMaxConnections) {
        _httpMaxConnections = httpMaxConnections;
    }

    public void setHttpMaxRetry(Integer httpMaxRetry) {
        _httpMaxRetry = httpMaxRetry;
    }

    public void setHttpRequestTimeout(Integer httpRequestTimeout) {
        _httpRequestTimeout = httpRequestTimeout;
    }

    public void setHttpRetryDelay(Integer httpRetryDelay) {
        _httpRetryDelay = httpRetryDelay;
    }

    public void setHttpSoTimeout(Integer httpSoTimeout) {
        _httpSoTimeout = httpSoTimeout;
    }

    public void setHttpUseMultipart(Boolean httpUseMultipart) {
        _httpUseMultipart = httpUseMultipart;
    }

}
