package net.strainovic.todo.solr.impl;


import net.strainovic.todo.solr.model.HttpClientConfiguration;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;

public class HttpClientFactory implements net.strainovic.todo.solr.HttpClientFactory {
    public HttpClientFactory() {
    }

    public static HttpClientFactory getInstance() {
        return new HttpClientFactory();
    }

    @Override
    public CloseableHttpClient createHttpClient(HttpClientConfiguration config) {
        if (config == null) {
            config = new HttpClientConfiguration();
        }
        HttpClientBuilder builder = HttpClients.custom();
        Builder reqConf = RequestConfig.custom();
        reqConf.setConnectTimeout(config.getConnectionTimeout());
        reqConf.setConnectionRequestTimeout(config.getRequestTimeout());
        reqConf.setSocketTimeout(config.getSoTimeout());
        reqConf.setCookieSpec(config.isHandleSessionId() ? CookieSpecs.BEST_MATCH
                : CookieSpecs.IGNORE_COOKIES);

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(config.getTotalConnections());
        connectionManager.setDefaultMaxPerRoute(config.getTotalConnections());
        int retry = config.getMaxRetry() < 0 ? 0 : config.getMaxRetry();
        final int delay = config.getRetryDelay();
        DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(
                retry, retry > 0) {
            @Override
            public boolean retryRequest(IOException exception,
                                        int executionCount, HttpContext context) {
                boolean flag = super.retryRequest(exception, executionCount,
                        context);
                if (flag) {
                    try {
                        Thread.sleep(delay);
                    } catch (InterruptedException e) {
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            }
        };

        builder.setRetryHandler(retryHandler);
        builder.setDefaultRequestConfig(reqConf.build());
        builder.setConnectionManager(connectionManager);
        return builder.build();
    }
}