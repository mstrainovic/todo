package net.strainovic.todo.webmvc.model;

import java.util.HashMap;

public class ModelMapResponse extends HashMap<String, Object> {
    public static final String SUCCESS = "success";
    private static final long serialVersionUID = -9060401825560202341L;

    public ModelMapResponse() {
        this.put(SUCCESS, Boolean.TRUE);
    }

    public void addObject(String key, Object value) {
        this.put(key, value);
    }

    public Boolean isSuccess() {
        return (Boolean) get(SUCCESS);
    }

}
