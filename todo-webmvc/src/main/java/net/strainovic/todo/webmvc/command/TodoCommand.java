package net.strainovic.todo.webmvc.command;

public class TodoCommand {

    private String note;

    private String ticket;


    public TodoCommand() {
    }

    public String getNote() {
        return note;
    }

    public String getTicket() {
        return ticket;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
