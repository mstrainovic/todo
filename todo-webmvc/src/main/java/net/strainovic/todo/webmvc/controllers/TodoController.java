package net.strainovic.todo.webmvc.controllers;

import net.strainovic.todo.dao.provider.TodoLoadProvider;
import net.strainovic.todo.model.Todo;
import net.strainovic.todo.model.dto.TodoDto;
import net.strainovic.todo.model.dto.converter.TicketConverter;
import net.strainovic.todo.model.dto.converter.TodoConverter;
import net.strainovic.todo.model.search.SearchRequest;
import net.strainovic.todo.model.util.LogHelper;
import net.strainovic.todo.service.TodoService;
import net.strainovic.todo.solr.RebuildSolrIndexService;
import net.strainovic.todo.webmvc.command.TodoCommand;
import net.strainovic.todo.webmvc.model.ModelMapResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;

import static net.strainovic.todo.webmvc.model.ControllersConstants.NOTES_KEY;
import static net.strainovic.todo.webmvc.model.ControllersConstants.NOTE_KEY;

@Controller
public class TodoController {

    private static final Log LOG = LogFactory.getLog(TodoController.class);

    @Autowired
    private RebuildSolrIndexService rebuildSolrIndexService;

    @Autowired
    private TodoService todoService;


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ModelMapResponse addTodo(@RequestBody TodoCommand command) {
        ModelMapResponse modelMapResponse = new ModelMapResponse();

        String note = command.getNote();

        if (StringUtils.isNotBlank(note)) {
            LogHelper.debug(LOG, String.format("Adding new todo with note: %s", note));

            Todo todo = new Todo();
            todo.setDateCreated(new Date());
            todo.setNote(note);

            Todo saved = getTodoService().save(todo);
            getRebuildSolrIndexService().rebuildIndex(saved.getTodoId());

            modelMapResponse.put(NOTE_KEY, TodoConverter.fromInternalToDto(saved));
        }

        return modelMapResponse;
    }

    @RequestMapping(value = "/destroyCompleted", method = RequestMethod.POST)
    @ResponseBody
    public void destroyCompleted() {
        //Add current user ID as restriction
        List<Todo> todos = getTodoService().loadAll(TodoLoadProvider.create().completed());

        if (CollectionUtils.isEmpty(todos)) {
            //Add user name or id to message
            LogHelper.warn(LOG, "Don't have completed todos for destroy");
            return;
        }

        todos.forEach(t -> {
            getTodoService().delete(t);
            getRebuildSolrIndexService().deleteById(t.getTodoId());
        });
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public void editTodo(@RequestBody TodoCommand command) {
        Long todoId = TicketConverter.fromDtoToInternal(command.getTicket());

        Todo todo = getTodoService().load(TodoLoadProvider.create().todoId(todoId));

        if (todo == null) {
            LogHelper.warn(LOG, String.format("Don't have todo with id %s to edit", todoId));
            return;
        }

        todo.setNote(command.getNote());
        todo.setDateCreated(new Date());

        getTodoService().update(todo);
        getRebuildSolrIndexService().rebuildIndex(todoId);
    }

    private RebuildSolrIndexService getRebuildSolrIndexService() {
        return rebuildSolrIndexService;
    }

    private TodoService getTodoService() {
        return todoService;
    }

    @RequestMapping(value = "/toggle", method = RequestMethod.POST)
    @ResponseBody
    public void markTodo(@RequestBody TodoCommand command) {
        Long todoId = TicketConverter.fromDtoToInternal(command.getTicket());

        Todo todo = getTodoService().load(TodoLoadProvider.create().todoId(todoId));

        if (todo == null) {
            LogHelper.warn(LOG, String.format("Don't have todo with id %s to mark", todoId));
            return;
        }

        todo.setCompleted(!todo.isCompleted());

        getTodoService().update(todo);
    }

    @RequestMapping(value = "/todo", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView openView() {
        return new ModelAndView("todo", "todoDto",
                null);
    }

    @RequestMapping(value = "/showAll", method = RequestMethod.POST)
    @ResponseBody
    public ModelMapResponse showTodos() {
        LogHelper.debug(LOG, "Show all todo's");

        ModelMapResponse modelMapResponse = new ModelMapResponse();

        SearchRequest searchRequest = new SearchRequest();

        List<Todo> todos = getTodoService().search(searchRequest);

        List<TodoDto> todoDtos = TodoConverter.fromInternalToDto(todos);
        modelMapResponse.put(NOTES_KEY, todoDtos);
        return modelMapResponse;
    }
}
