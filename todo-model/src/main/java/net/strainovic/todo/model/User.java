package net.strainovic.todo.model;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

@Entity
@BatchSize(size = 100)
@Table(name = "USER")
public class User implements Identifiable<Long> {

    private String email;
    private String password;
    private Long userId;
    private String userName;

    @Column(name = "EMAIL", unique = true)
    public String getEmail() {
        return email;
    }

    @Override
    public Long getIdentifier() {
        return getUserId();
    }

    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    public Long getUserId() {
        return userId;
    }

    @Column(name = "USERNAME", unique = true)
    public String getUserName() {
        return userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void setIdentifier(Long identifier) {
        setUserId(identifier);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
