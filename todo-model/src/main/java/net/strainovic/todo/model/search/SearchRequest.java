package net.strainovic.todo.model.search;


public class SearchRequest {

    private String todoText;

    public String getTodoText() {
        return todoText;
    }

    public void setTodoText(String todoText) {
        this.todoText = todoText;
    }
}
