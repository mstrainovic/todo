package net.strainovic.todo.model.dto;

import java.util.Date;

public class TodoDto {

    private boolean completed = false;
    private Date dateCreated;
    private String note;
    private String ticket;

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getNote() {
        return note;
    }

    public String getTicket() {
        return ticket;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }


    public void setNote(String note) {
        this.note = note;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
