package net.strainovic.todo.model;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.Date;

@Entity
@BatchSize(size = 1000)
@Table(name = "TODO")
public class Todo implements Identifiable<Long> {

    private boolean completed = false;
    private Date dateCreated;
    private String note;
    private Long todoId;
    private User user;

    @Column(name = "DATE_CREATED", nullable = false)
    public Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public Long getIdentifier() {
        return getTodoId();
    }

    @Column(name = "NOTE", nullable = false)
    public String getNote() {
        return note;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TODO_ID")
    public Long getTodoId() {
        return todoId;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }

    @Column(name = "COMPLETED", nullable = false)
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public void setIdentifier(Long identifier) {
        setTodoId(identifier);
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
