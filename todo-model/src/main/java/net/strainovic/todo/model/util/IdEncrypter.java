package net.strainovic.todo.model.util;

import net.strainovic.todo.model.cript.DesEncrypter;
import net.strainovic.todo.model.cript.Encrypter;
import org.apache.commons.lang.StringUtils;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class IdEncrypter {
    private static final String GENERIC_PREFIX = "Generic;TDC;"; //$NON-NLS-1$
    private static final String GENERIC_SUFFIX = ";COM"; //$NON-NLS-1$
    private static final SecretKey _1KEY = new SecretKeySpec(new byte[]{10,
            20, 30, 40, 50, 60, 70, 80}, "DES"); //$NON-NLS-1$
    private static final Pattern _genericRegex = Pattern
            .compile("Generic;TDC;(\\d+);COM"); //$NON-NLS-1$
    private static IdEncrypter _idEncrypter = new IdEncrypter();
    private Encrypter _encrypter = new DesEncrypter(_1KEY);

    public static Long decryptGenericId(String message) {
        return _idEncrypter.decryptId(message, _genericRegex);
    }

    public static String encryptGenericId(long id) {
        return _idEncrypter.encryptId(id, GENERIC_PREFIX, GENERIC_SUFFIX);
    }

    protected Long decryptId(String message, Pattern regex) {
        if (StringUtils.isEmpty(message)) {
            return new Long(0);
        }

        String text = getEncrypter().decrypt(message);

        Matcher matcher = regex.matcher(text);

        if (matcher.find()) {
            return Long.parseLong(matcher.group(1));
        } else {
            throw new RuntimeException("Invalid ticket.");
        }
    }

    protected String encryptId(long id, String prefix, String suffix) {
        StringBuffer buffer = new StringBuffer(prefix); //$NON-NLS-1$
        buffer.append(id);
        buffer.append(suffix); //$NON-NLS-1$

        return getEncrypter().encrypt(buffer.toString());
    }

    public Encrypter getEncrypter() {
        return _encrypter;
    }

    public void setEncrypter(Encrypter encrypter) {
        _encrypter = encrypter;
    }
}
