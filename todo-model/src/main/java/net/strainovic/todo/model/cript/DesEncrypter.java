package net.strainovic.todo.model.cript;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import java.io.UnsupportedEncodingException;

public class DesEncrypter implements Encrypter {
    private Cipher _dcipher;
    private Cipher _ecipher;

    public DesEncrypter(SecretKey key) {
        try {
            _ecipher = Cipher.getInstance("DES");
            _dcipher = Cipher.getInstance("DES");

            _ecipher.init(Cipher.ENCRYPT_MODE, key);
            _dcipher.init(Cipher.DECRYPT_MODE, key);
        } catch (javax.crypto.NoSuchPaddingException | java.security.InvalidKeyException | java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException("An exception occured while encrypting data.", e);
        }
    }

    public String decrypt(String str) {
        try {
            byte[] dec = Base64.decodeBase64(str.getBytes("ASCII"));

            // Decrypt
            byte[] utf8 = _dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (javax.crypto.BadPaddingException | UnsupportedEncodingException | IllegalBlockSizeException e) {
            throw new RuntimeException("An exception occured while decripting data.", e);
        }
    }

    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = _ecipher.doFinal(utf8);

            return new String(Base64.encodeBase64(enc), "ASCII");
        } catch (javax.crypto.BadPaddingException | IllegalBlockSizeException | UnsupportedEncodingException e) {
            throw new RuntimeException("An exception occured while decripting data.", e);
        }
    }
}