package net.strainovic.todo.model.dto.converter;

import net.strainovic.todo.model.util.IdEncrypter;

public class TicketConverter {

    public static Long fromDtoToInternal(String dtoTicket) {
        if (dtoTicket == null) {
            return null;
        }
        return IdEncrypter.decryptGenericId(dtoTicket);
    }

    public static String fromInternalToDto(Long internalEntity) {
        if (internalEntity == null) {
            return null;
        }
        return IdEncrypter.encryptGenericId(internalEntity);
    }

}
