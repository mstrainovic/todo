package net.strainovic.todo.model.dto.converter;

import net.strainovic.todo.model.Todo;
import net.strainovic.todo.model.dto.TodoDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TodoConverter {

    public static TodoDto fromInternalToDto(Todo todo) {
        TodoDto todoDto = new TodoDto();
        todoDto.setTicket(TicketConverter.fromInternalToDto(todo.getTodoId()));
        todoDto.setNote(todo.getNote());
        todoDto.setDateCreated(todo.getDateCreated());
        todoDto.setCompleted(todo.isCompleted());

        return todoDto;
    }

    public static List<TodoDto> fromInternalToDto(Collection<Todo> todos) {
        return todos.stream().map(TodoConverter::fromInternalToDto)
                .collect(Collectors.toList());
    }

}
