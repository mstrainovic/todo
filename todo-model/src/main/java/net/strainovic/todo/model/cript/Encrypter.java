package net.strainovic.todo.model.cript;

public interface Encrypter {
    String decrypt(String text);

    String encrypt(String text);
}
