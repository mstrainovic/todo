/**
 *
 */
package net.strainovic.todo.model;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> extends Serializable {

    T getIdentifier();

    void setIdentifier(T identifier);
}
