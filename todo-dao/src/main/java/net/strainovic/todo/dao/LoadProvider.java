package net.strainovic.todo.dao;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface LoadProvider<E> extends Serializable {

    List<E> resultList(Session session);

    List<Long> resultListId(Session session);

    E singleResult(Session session);

}
