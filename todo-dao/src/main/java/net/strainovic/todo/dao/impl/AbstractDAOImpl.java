package net.strainovic.todo.dao.impl;

import net.strainovic.todo.dao.GenericDAO;
import net.strainovic.todo.dao.LoadProvider;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.List;

public abstract class AbstractDAOImpl<T, ID extends Serializable> implements
        GenericDAO<T, ID> {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void delete(final T entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public void flush() {
        getCurrentSession().flush();
    }

    protected Session getCurrentSession() {
        Session session = getSessionFactory().getCurrentSession();
        return session;
    }

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public T load(LoadProvider<T> loadProvider) {
        return loadProvider.singleResult(getCurrentSession());
    }

    @Override
    public List<T> loadAll(LoadProvider<T> loadProvider) {
        return loadProvider.resultList(getCurrentSession());
    }

    @Override
    public List<Long> loadIds(LoadProvider<T> loadProvider) {
        return loadProvider.resultListId(getCurrentSession());
    }

    @Override
    public final T save(final T entity) {
        getCurrentSession().save(entity);
        return entity;
    }

    @Override
    public final void update(final T entity) {
        getCurrentSession().update(entity);
    }
}
