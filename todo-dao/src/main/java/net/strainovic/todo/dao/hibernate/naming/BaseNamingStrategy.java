package net.strainovic.todo.dao.hibernate.naming;

import org.hibernate.cfg.DefaultNamingStrategy;

public class BaseNamingStrategy extends DefaultNamingStrategy {

    private static final long serialVersionUID = 1841887743417098761L;

    private String _tablePrefix;

    @Override
    public String collectionTableName(String ownerEntity,
                                      String ownerEntityTable, String associatedEntity,
                                      String associatedEntityTable, String propertyName) {

        return new StringBuffer(getTablePrefix()).append(
                super.collectionTableName(ownerEntity, ownerEntityTable,
                        associatedEntity, associatedEntityTable, propertyName))
                .toString();
    }

    public String getTablePrefix() {
        return _tablePrefix;
    }

    public void setTablePrefix(String tablePrefix) {
        _tablePrefix = tablePrefix;
    }

    @Override
    public String tableName(String tableName) {
        return new StringBuffer(getTablePrefix()).append(
                super.tableName(tableName)).toString();
    }

}
