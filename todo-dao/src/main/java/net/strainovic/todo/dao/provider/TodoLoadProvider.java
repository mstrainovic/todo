package net.strainovic.todo.dao.provider;


import net.strainovic.todo.dao.hibernate.AbstractLoadProvider;
import net.strainovic.todo.model.Todo;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;

public class TodoLoadProvider extends AbstractLoadProvider<Todo> {

    private boolean loadUser;

    public static TodoLoadProvider create() {
        return new TodoLoadProvider();
    }

    public TodoLoadProvider completed() {
        addCriterion(Restrictions.eq("completed", true));

        return this;
    }

    @Override
    protected Class<Todo> getEntityPersistenceClass() {
        return Todo.class;
    }

    private boolean isLoadUser() {
        return loadUser;
    }

    public TodoLoadProvider loadUser() {
        loadUser = Boolean.TRUE;

        return this;
    }

    public TodoLoadProvider note(String note) {
        addCriterion(Restrictions.eq("note", note));

        return this;
    }

    @Override
    protected void postLoadInitialize(Todo todo) {
        if (isLoadUser()) {
            Hibernate.initialize(todo.getUser());
        }
    }

    public TodoLoadProvider todoId(Long... todoIds) {
        addCriterion(Restrictions.in("id", todoIds));

        return this;
    }

    public TodoLoadProvider todoId(Collection<Long> todoIds) {
        addCriterion(Restrictions.in("id", todoIds));

        return this;
    }

    public TodoLoadProvider userId(Long userId) {
        addCriterion(Restrictions.eq("user.userId", userId));

        return this;
    }
}
