package net.strainovic.todo.dao.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public final class ChunkedExecutionHelper {

    public static final int DEFAULT_CHUNK_SIZE = 1000;

    public static <E> void executeChuncked(Collection<E> arguments,
                                           ChunkedListExecutionCallback<E> callback, int chunckSize) {
        List<E> chunck = new ArrayList<>();

        for (E item : arguments) {
            chunck.add(item);

            if (chunck.size() == chunckSize) {
                callback.execute(chunck);
                chunck.clear();
            }
        }

        if (chunck.size() > 0) {
            callback.execute(chunck);
        }
    }

    public static <E> void executeChuncked(Collection<E> arguments,
                                           ChunkedListExecutionCallback<E> callback) {
        executeChuncked(arguments, callback, DEFAULT_CHUNK_SIZE);
    }

    public interface ChunkedArrayExecutionCallback<E> {

        void execute(E[] chunck);
    }

    public interface ChunkedListExecutionCallback<E> {

        void execute(List<E> chunck);
    }
}