package net.strainovic.todo.dao;

import net.strainovic.todo.model.User;

public interface UserDAO extends GenericDAO<User, Long> {
}
