package net.strainovic.todo.dao.provider;

import net.strainovic.todo.dao.hibernate.AbstractLoadProvider;
import net.strainovic.todo.model.User;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;

public class UserLoadProvider extends AbstractLoadProvider<User> {

    public static UserLoadProvider create() {
        return new UserLoadProvider();
    }

    @Override
    protected Class<User> getEntityPersistenceClass() {
        return User.class;
    }

    @Override
    protected void postLoadInitialize(User entity) {

    }

    public UserLoadProvider userId(Collection<Long> userIds) {
        addCriterion(Restrictions.in("id", userIds));

        return this;
    }

    public UserLoadProvider userId(Long... userIds) {
        addCriterion(Restrictions.in("id", userIds));

        return this;
    }

    public UserLoadProvider userName(String userName) {
        addCriterion(Restrictions.eq("userName", userName));

        return this;
    }
}
