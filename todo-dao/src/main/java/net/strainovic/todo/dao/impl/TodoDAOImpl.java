package net.strainovic.todo.dao.impl;

import net.strainovic.todo.dao.TodoDAO;
import net.strainovic.todo.model.Todo;
import org.springframework.stereotype.Repository;

@Repository("todoDAO")
public class TodoDAOImpl extends AbstractDAOImpl<Todo, Long> implements TodoDAO {

}
