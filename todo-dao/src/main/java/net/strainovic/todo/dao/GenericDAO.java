package net.strainovic.todo.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, ID> extends Serializable {

    void delete(T entity);

    void flush();

    T load(LoadProvider<T> loadProvider);

    List<T> loadAll(LoadProvider<T> loadProvider);

    List<Long> loadIds(LoadProvider<T> loadProvider);

    T save(T entity);

    void update(T entity);

}