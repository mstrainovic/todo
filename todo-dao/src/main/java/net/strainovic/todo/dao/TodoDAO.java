package net.strainovic.todo.dao;

import net.strainovic.todo.model.Todo;

public interface TodoDAO extends GenericDAO<Todo, Long> {
}
