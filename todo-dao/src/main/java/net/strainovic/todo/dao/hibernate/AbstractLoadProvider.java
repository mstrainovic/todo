package net.strainovic.todo.dao.hibernate;

import net.strainovic.todo.dao.LoadProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.Validate;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractLoadProvider<E> implements LoadProvider<E> {

    public static final int MAX_PAGE_SIZE = 1000;

    private static final long serialVersionUID = 1096370837484327040L;

    private List<Alias> aliases;
    private List<Criterion> criterions;
    private List<Order> orders;

    private int pagePointer = 0;
    private Integer pageSize;

    protected AbstractLoadProvider() {
        criterions = new ArrayList<>();
        orders = new ArrayList<>();
        aliases = new ArrayList<>();
    }

    protected void addAlias(Alias alias) {
        aliases.add(alias);
    }

    protected void addCriterion(Criterion criterion) {
        criterions.add(criterion);
    }

    protected void addOrder(Order order) {
        orders.add(order);
    }

    private List<Alias> getAliases() {
        return aliases;
    }

    private List<Criterion> getCriterions() {
        return criterions;
    }

    protected abstract Class<E> getEntityPersistenceClass();

    private List<Order> getOrders() {
        return orders;
    }

    private int getPagePointer() {
        return pagePointer;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void incrementPagePointer() {
        pagePointer++;
    }

    public void pagePointer(int pagePointer) {
        Validate.isTrue(pagePointer >= 0);

        this.pagePointer = pagePointer;
    }

    public void pageSize(int pageSize) {
        Validate.isTrue(pageSize <= MAX_PAGE_SIZE);

        this.pageSize = pageSize;
    }

    protected abstract void postLoadInitialize(E entity);

    @Override
    @SuppressWarnings("unchecked")
    public List<E> resultList(Session session) {
        Criteria criterion = session
                .createCriteria(getEntityPersistenceClass());

        for (Alias alias : getAliases()) {
            criterion.createAlias(alias.getAssociationPath(), alias.getAlias(),
                    alias.getJoinType());
        }

        getCriterions().forEach(criterion::add);

        getOrders().forEach(criterion::addOrder);

        Integer pageSize = getPageSize();
        if (pageSize != null) {
            criterion.setFirstResult(pageSize * getPagePointer());
            criterion.setMaxResults(pageSize);
        }

        List<E> result;
        if (getAliases().isEmpty()) {
            result = criterion.list();
        } else {
            criterion.setProjection(Projections.distinct(Projections.id()));

            List<?> ids = criterion.list();

            if (!ids.isEmpty()) {
                Criteria itemsCriterion = session
                        .createCriteria(getEntityPersistenceClass());

                itemsCriterion.add(Restrictions.in("id", ids));

                result = itemsCriterion.list();
            } else {
                result = new ArrayList<>();
            }
        }

        if (CollectionUtils.isNotEmpty(result)) {
            result.forEach(this::postLoadInitialize);
        }

        return result;
    }

    @Override
    public List<Long> resultListId(Session session) {
        Criteria criterion = session
                .createCriteria(getEntityPersistenceClass());

        for (Alias alias : getAliases()) {
            criterion.createAlias(alias.getAssociationPath(), alias.getAlias(),
                    alias.getJoinType());
        }

        getCriterions().forEach(criterion::add);

        getOrders().forEach(criterion::addOrder);

        Integer pageSize = getPageSize();
        if (pageSize != null) {
            criterion.setFirstResult(pageSize * getPagePointer());
            criterion.setMaxResults(pageSize);
        }

        List<Long> result;
        if (getAliases().isEmpty()) {
            criterion.setProjection(Projections.id());
            result = criterion.list();
        } else {
            criterion.setProjection(Projections.distinct(Projections.id()));
            result = criterion.list();
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E singleResult(Session session) {
        Criteria criterion = session
                .createCriteria(getEntityPersistenceClass());

        for (Alias alias : getAliases()) {
            criterion.createAlias(alias.getAssociationPath(), alias.getAlias(),
                    alias.getJoinType());
        }
        getCriterions().forEach(criterion::add);

        E result = (E) criterion.uniqueResult();

        if (result != null) {
            postLoadInitialize(result);
        }

        return result;
    }

}
