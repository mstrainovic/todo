package net.strainovic.todo.dao.hibernate;

import org.hibernate.sql.JoinType;

public class Alias {

    private String _alias;

    private String _associationPath;

    private JoinType _joinType;

    public Alias(String associationPath, String alias, JoinType joinType) {
        super();
        _associationPath = associationPath;
        _alias = alias;
        _joinType = joinType;
    }

    public String getAlias() {
        return _alias;
    }

    public String getAssociationPath() {
        return _associationPath;
    }

    public JoinType getJoinType() {
        return _joinType;
    }

    public void setAlias(String alias) {
        _alias = alias;
    }

    public void setAssociationPath(String associationPath) {
        _associationPath = associationPath;
    }

    public void setJoinType(JoinType joinType) {
        _joinType = joinType;
    }
}
