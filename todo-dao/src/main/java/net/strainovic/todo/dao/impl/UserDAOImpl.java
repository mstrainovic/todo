package net.strainovic.todo.dao.impl;

import net.strainovic.todo.dao.UserDAO;
import net.strainovic.todo.model.User;
import org.springframework.stereotype.Repository;

@Repository("userDAO")
public class UserDAOImpl extends AbstractDAOImpl<User, Long> implements UserDAO {
}
