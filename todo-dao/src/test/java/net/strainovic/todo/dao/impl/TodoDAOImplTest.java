package net.strainovic.todo.dao.impl;

import net.strainovic.todo.dao.AbstractDAOTest;
import net.strainovic.todo.dao.TodoDAO;
import net.strainovic.todo.dao.provider.TodoLoadProvider;
import net.strainovic.todo.model.Todo;
import net.strainovic.todo.model.User;
import net.strainovic.todo.model.test.annotations.TestCase;
import net.strainovic.todo.model.test.annotations.TestSuite;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@TestSuite("todo")
public class TodoDAOImplTest extends AbstractDAOTest {

    @Autowired
    private TodoDAO todoDAO;

    @Test
    public void CRUDTest() {
        List<Todo> todos = getTodoDAO().loadAll(TodoLoadProvider.create());
        Assert.assertEquals(0, todos.size());

        Todo todo = new Todo();
        todo.setNote("note");
        todo.setDateCreated(new Date());

        getTodoDAO().save(todo);

        flushSession();
        clearSession();

        todos = getTodoDAO().loadAll(TodoLoadProvider.create());

        Assert.assertEquals(1, todos.size());
        todo = todos.get(0);

        Assert.assertEquals("note", todo.getNote());

        todo.setNote("note2");
        getTodoDAO().update(todo);

        flushSession();
        clearSession();

        todos = getTodoDAO().loadAll(TodoLoadProvider.create());

        Assert.assertEquals(1, todos.size());
        todo = todos.get(0);

        Assert.assertEquals("note2", todo.getNote());

        getTodoDAO().delete(todo);

        flushSession();
        clearSession();

        todos = getTodoDAO().loadAll(TodoLoadProvider.create());
        Assert.assertEquals(0, todos.size());
    }

    @Test
    public void cascadeSaveTest() {
        Todo todo = new Todo();
        todo.setNote("Some random note");

        User user = new User();
        user.setUserName("mstrainovic");
        user.setEmail("marko.strainovic@gmail.com");

        todo.setUser(user);
        todo.setDateCreated(new Date());

        getTodoDAO().save(todo);

        flushSession();
        clearSession();

        todo = getTodoDAO().load(TodoLoadProvider.create().note("Some random note"));

        Assert.assertNotNull(todo.getUser());
        Assert.assertEquals("mstrainovic", todo.getUser().getUserName());
    }

    @Test
    @TestCase("todoBaseSet")
    public void findAllIdsTest() {
        List<Long> ids = getTodoDAO().loadIds(TodoLoadProvider.create());

        Assert.assertEquals(2, ids.size());
    }

    @Test
    @TestCase("todoBaseSet")
    public void findAllTest() {
        List<Todo> todos = getTodoDAO().loadAll(TodoLoadProvider.create());

        Assert.assertEquals(2, todos.size());
    }

    @Test
    @TestCase("todoBaseSet")
    public void findByIdTest() {
        Todo todo = getTodoDAO().load(TodoLoadProvider.create().todoId(2L));

        Assert.assertEquals("Go to football", todo.getNote());
    }

    @Test
    @TestCase("todoBaseSet")
    public void findCompletedTest() {
        List<Todo> todos = getTodoDAO().loadAll(TodoLoadProvider.create().completed());

        Assert.assertEquals(1, todos.size());
        Assert.assertTrue(todos.get(0).isCompleted());
    }

    @Test
    @TestCase("todoBaseSet")
    public void findUsersTest() {
        List<Todo> todos = getTodoDAO().loadAll(TodoLoadProvider.create().userId(1L));

        Assert.assertEquals(1, todos.size());
        Assert.assertEquals(Long.valueOf(1), todos.get(0).getUser().getUserId());
    }

    private TodoDAO getTodoDAO() {
        return todoDAO;
    }
}