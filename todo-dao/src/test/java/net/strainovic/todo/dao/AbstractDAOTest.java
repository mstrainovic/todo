package net.strainovic.todo.dao;

import net.strainovic.todo.model.test.annotations.TestCase;
import net.strainovic.todo.model.test.annotations.TestSuite;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.lang.reflect.Method;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:net/strainovic/todo/dao/spring/applicationContext-dao.xml"})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class})
@Transactional
public abstract class AbstractDAOTest {

    @Rule
    public TestName _testMethodName = new TestName();
    @Autowired
    private DataSource _dataSource;

    private DbUnitHelper _dbUnitHelper;
    @Autowired
    private SessionFactory sessionFactory;

    @BeforeClass
    public static void setUpClass() throws Exception {
        SimpleNamingContextBuilder builder = SimpleNamingContextBuilder
                .emptyActivatedContextBuilder();

        SingleConnectionDataSource ds = new SingleConnectionDataSource();

        ds.setDriverClassName("net.sf.log4jdbc.DriverSpy");
        ds.setUrl("jdbc:log4jdbc:h2:mem:test;create=true");

        ds.setSuppressClose(true);

        builder.bind("java:comp/env/jdbc/todo", ds);
    }

    private void addDataSetFromAnnotation() throws Exception {
        Class<?> clazz = this.getClass();
        TestSuite classAnnotation = clazz.getAnnotation(TestSuite.class);
        Method testMethod = clazz.getMethod(_testMethodName.getMethodName(),
                new Class<?>[]{});
        TestCase methodAnnotation = testMethod.getAnnotation(TestCase.class);

        if (methodAnnotation != null && classAnnotation != null) {

            FlatXmlDataSetBuilder builderData = new FlatXmlDataSetBuilder();
            String pathFromAnotation = buildTestCasePath(classAnnotation,
                    methodAnnotation);
            String baseDataSetName = "data-sets/annotations/"
                    + pathFromAnotation;
            IDataSet dataSet = builderData.build(getClass().getClassLoader()
                    .getResourceAsStream(baseDataSetName));
            _dbUnitHelper.setupDatabase(dataSet);
        }
    }

    private String buildTestCasePath(TestSuite classAnnotation,
                                     TestCase methodAnnotation) {
        StringBuilder path = new StringBuilder();

        path.append(classAnnotation.value());
        path.append("/");
        path.append(methodAnnotation.value());
        path.append(".xml");

        return path.toString();
    }

    protected void clearSession() {
        Session session = getCurrentSession();

        Assert.assertNotNull(session);

        session.clear();
    }

    protected void flushSession() {
        Session session = getCurrentSession();

        Assert.assertNotNull(session);

        session.flush();
    }

    protected Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }

    public DataSource getDataSource() {
        return _dataSource;
    }

    public DbUnitHelper getDbUnitHelper() {
        return _dbUnitHelper;
    }

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setDataSource(DataSource dataSource) {
        _dataSource = dataSource;
    }

    public void setDbUnitHelper(DbUnitHelper dbUnitHelper) {
        _dbUnitHelper = dbUnitHelper;
    }

    @Before
    public void setUp() throws Exception {

        _dbUnitHelper = new DbUnitHelper(getDataSource());

        String cleanDataSetName = "data-sets/cleanDataSet.xml";

        FlatXmlDataSetBuilder builderClean = new FlatXmlDataSetBuilder();
        builderClean.setColumnSensing(true);
        IDataSet cleanDataSet = builderClean.build(getClass().getClassLoader()
                .getResourceAsStream(cleanDataSetName));

        _dbUnitHelper.cleanDatabase(cleanDataSet);

        addDataSetFromAnnotation();

    }
}