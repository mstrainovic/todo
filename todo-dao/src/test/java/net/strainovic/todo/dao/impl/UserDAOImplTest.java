package net.strainovic.todo.dao.impl;

import net.strainovic.todo.dao.AbstractDAOTest;
import net.strainovic.todo.dao.UserDAO;
import net.strainovic.todo.dao.provider.UserLoadProvider;
import net.strainovic.todo.model.User;
import net.strainovic.todo.model.test.annotations.TestSuite;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@TestSuite("user")
public class UserDAOImplTest extends AbstractDAOTest {

    @Autowired
    private UserDAO userDAO;

    @Test
    public void CRUDTest() {
        List<User> users = getUserDAO().loadAll(UserLoadProvider.create());
        Assert.assertEquals(0, users.size());

        User user = new User();
        user.setEmail("marko.strainovic@gmail.com");
        user.setUserName("mstrainovic");

        getUserDAO().save(user);

        flushSession();
        clearSession();

        users = getUserDAO().loadAll(UserLoadProvider.create());
        Assert.assertEquals(1, users.size());
        user = users.get(0);

        Assert.assertEquals("mstrainovic", user.getUserName());

        user.setUserName("mstrainovic2");
        getUserDAO().update(user);

        flushSession();
        clearSession();

        users = getUserDAO().loadAll(UserLoadProvider.create());

        Assert.assertEquals(1, users.size());
        user = users.get(0);

        Assert.assertEquals("mstrainovic2", user.getUserName());

        getUserDAO().delete(user);

        flushSession();
        clearSession();

        users = getUserDAO().loadAll(UserLoadProvider.create());
        Assert.assertEquals(0, users.size());
    }

    private UserDAO getUserDAO() {
        return userDAO;
    }
}
