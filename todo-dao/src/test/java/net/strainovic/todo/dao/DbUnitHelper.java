package net.strainovic.todo.dao;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbUnitHelper {

    private static int OUTPUT_BUFFER_SIZE = 1024 * 1024;
    private final DataSource _dataSource;

    public DbUnitHelper(DataSource dbSource) {
        _dataSource = dbSource;
    }

    public void cleanDatabase(IDataSet cleanDataSet) throws Exception {
        IDatabaseConnection connection = getDatabaseConnection();

        switchIntegrityVerification(connection, false);

        DatabaseOperation.DELETE_ALL.execute(connection, cleanDataSet);

        switchIntegrityVerification(connection, true);

        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void executeQuery(String query, OutputStream outputStream) {
        IDatabaseConnection dbConn = getDatabaseConnection();
        try {
            QueryDataSet dataSet = new QueryDataSet(dbConn);
            dataSet.addTable("RESULT", query);

            FlatXmlDataSet.write(dataSet, outputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                dbConn.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void exportDatabase(String outputFileName) {
        try {
            IDatabaseConnection conn = getDatabaseConnection();
            IDataSet fullDataSet = conn.createDataSet();

            FileOutputStream fileOutputStream = new FileOutputStream(outputFileName);
            FlatXmlDataSet.write(fullDataSet, new BufferedOutputStream(fileOutputStream, OUTPUT_BUFFER_SIZE));
            fileOutputStream.close();

            conn.close();
        } catch (DataSetException e) {
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public DataSource getDataSource() {
        return _dataSource;
    }

    public IDatabaseConnection getDatabaseConnection() {

        IDatabaseConnection connection;
        try (Connection jdbcConnection = getDataSource().getConnection()) {

            connection = new DatabaseConnection(jdbcConnection);
        } catch (DatabaseUnitException | SQLException e) {
            throw new RuntimeException(e);
        }
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());

        return connection;
    }

    public void setupDatabase(IDataSet initDataSet) throws Exception {
        IDatabaseConnection connection = getDatabaseConnection();

        switchIntegrityVerification(connection, false);

        DatabaseOperation.INSERT.execute(connection, initDataSet);

        switchIntegrityVerification(connection, true);

        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void switchIntegrityVerification(IDatabaseConnection connection, boolean turnOn) {
        try (Connection sqlConnection = connection.getConnection()) {
            PreparedStatement statement = sqlConnection.prepareStatement("SET REFERENTIAL_INTEGRITY " //$NON-NLS-1$
                    + Boolean.toString(turnOn).toUpperCase());

            statement.execute();

            statement.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private enum DriverEnum {
        DEFAULT, H2, HSQL, MSSQL, MYSQL;
    }

}
